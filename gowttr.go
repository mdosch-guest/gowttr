/* Copyright Martin Dosch
   Licensed under the "MIT License" */

package main

import (
	"encoding/json"
	"errors"
	"io"
	"log"
	"net/http"
	"os"
	"os/user"
	"strings"

	"github.com/xmppo/go-xmpp"
)

func getWeather(location string, language string) (weather string, err error) {
	var wttrURL string

	// Remove leading and trailing spaces (often caused by autocompletion).
	location = strings.Trim(location, " ")

	// Replace spaces with +.
	location = strings.ReplaceAll(location, " ", "+")

	client := &http.Client{}

	// Use language specific URL if language is set in the XMPP message
	if language != "" {
		// Split at "_" and use the first slice to turn e.g. "de_DE.UTF-8"
		// into de.
		language = strings.Split(language, "_")[0]
		// Split at "-" and use the first slice to turn e.g. "de-DE"
		// into de.
		language = strings.Split(language, "-")[0]
		wttrURL = "https://" + language + ".wttr.in/" + location + "?0T"
	} else {
		wttrURL = "https://wttr.in/" + location + "?0T"
	}

	req, err := http.NewRequest("GET", wttrURL, nil)
	if err != nil {
		return weather, err
	}

	// Pretend to be cURL as wttr.in otherwise delivers html.
	req.Header.Set("User-Agent", "curl/7.88.1")

	resp, err := client.Do(req)
	if err != nil {
		return weather, err
	}

	defer resp.Body.Close()
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return weather, err
	}

	wttrOutput := string(body)

	// Don't parse output if no weather data is available or if it's the
	// special output for the moon.
	if strings.Contains(wttrOutput, "https://twitter.com/igor_chubin") ||
		strings.ToLower(location) == "moon" ||
		strings.HasPrefix(strings.ToLower(location), "moon@") {
		output := "```\n" + wttrOutput + "\n```"
		return output, err
	}

	if strings.Contains(wttrOutput, "<title>404 Not Found</title>") ||
		resp.StatusCode == 404 {
		return "Sorry, location not found.", err
	}

	weatherData := strings.Split(wttrOutput, "\n")

	var asciiArt, text string

	// Parse the wttr.in output.
	for i, lines := range weatherData {
		switch i {
		// The first line contains the name of the found location.
		// If the location can't be found it falls back to "Oymyakon, Russia".
		// Therefore processing of output is stopped when "Oymyakon, Russia"
		// is present but "Oymyakon" is not in the search string.
		case 0:
			if strings.Contains(lines, "Oymyakon, Russia") &&
				!strings.Contains(strings.ToLower(location), "oymyakon") {
				return "Sorry, location not found.", err
			}
			weather = "```\n" + lines + "\n\n"
			// Separate the ascii art on the left from the text on
			// the right and place the text under the ascii art
			// afterwards to increase readability on small screens.
		case 2, 3, 4, 5, 6:
			// Ascii art contains one character less for every occurrence
			// of ⚡.
			asciiArtLength := 15 - strings.Count(lines, "⚡")
			a := []rune(lines)
			for k, r := range a {
				if k < asciiArtLength {
					asciiArt += string(r)
					if k == asciiArtLength-1 {
						asciiArt += "\n"
					}
				} else {
					text += string(r)
					if k == len(a)-1 {
						text += "\n"
					}
				}
			}
		}
	}

	weather = weather + asciiArt + "\n" + text +
		"\nForecast: " + "https://wttr.in/" +
		location + "\n```"

	return weather, err
}

func main() {
	var err error

	type configuration struct {
		Address   string
		BotJid    string
		Password  string
		DirectTLS bool
		Debug     bool
		PLAIN	  bool
	}

	var configpath string

	// Get systems user config path.
	osConfigDir := os.Getenv("$XDG_CONFIG_HOME")
	if osConfigDir != "" {
		// Create configpath if not yet existing.
		configpath = osConfigDir + "/.config/gowttr"
		if _, err := os.Stat(configpath + "config.json"); os.IsNotExist(err) {
			err = os.MkdirAll(configpath, 0o700)
			if err != nil {
				log.Fatal("Error: ", err)
			}
		}

	} else { // Get the current user.
		curUser, err := user.Current()
		if err != nil {
			log.Fatal("Error: ", err)
			return
		}
		// Get home directory.
		home := curUser.HomeDir

		if home == "" {
			log.Fatal("Error: No home directory available.")
			return
		}

		// Create configpath if not yet existing.
		configpath = home + "/.config/gowttr/"
		if _, err := os.Stat(configpath + "config.json"); os.IsNotExist(err) {
			err = os.MkdirAll(configpath, 0o700)
			if err != nil {
				log.Fatal("Error: ", err)
			}
		}

	}

	// Check that config file is existing.
	if _, err := os.Stat(configpath + "config.json"); os.IsNotExist(err) {
		log.Fatal("Error: ", err)
	}

	// Read configuration file into variable config.
	file, _ := os.Open(configpath + "config.json")
	decoder := json.NewDecoder(file)
	config := configuration{}
	if err := decoder.Decode(&config); err != nil {
		file.Close()
		log.Fatal("Error: ", err)
	}
	file.Close()

	options := xmpp.Options{
		Host:        config.Address,
		User:        config.BotJid,
		Password:    config.Password,
		StartTLS:    !config.DirectTLS,
		NoTLS:       !config.DirectTLS,
		Debug:       config.Debug,
		SSDP:        true,
		NoPLAIN:     !config.PLAIN,
		UserAgentSW: "gowttr",
	}

	client, err := options.NewClient()
	if err != nil {
		log.Fatalf("%+v", err)
	}

	for {
		received, err := client.Recv()
		switch v := received.(type) {
		case xmpp.Chat:
			// Try to get mutual subscription as otherwise
			// some firewalls my block the replies.
			client.ApproveSubscription(v.Remote)
			client.RequestSubscription(v.Remote)
			reply, err := handleMessage(v)
			if err != nil {
				errorHandler(err)
			} else {
				_, err = client.Send(reply)
				if err != nil {
					errorHandler(err)
				}
			}
		default:
			if err != nil {
				errorHandler(err)
			}
		}
	}
}

func handleMessage(msg xmpp.Chat) (xmpp.Chat, error) {
	var replyBody string
	// Ignore messages with empty body.
	if msg.Text == "" {
		return msg, errors.New("message body was empty")
	}

	// Only use the first line if a multi line message is received.
	msgContent := strings.Split(msg.Text, "\n")[0]

	switch strings.ToLower(msgContent) {
	case "!help":
		replyBody = "Usage:\n" +
			"!help: Show this message.\n" +
			"!license: Show license information.\n" +
			"!source: Show link to source code repo.\n" +
			"location: Show the current weather at location."
	case "!license":
		replyBody = "MIT License: https://salsa.debian.org/mdosch/gowttr/-/raw/master/LICENSE"
	case "!source":
		replyBody = "https://salsa.debian.org/mdosch/gowttr"
	default:
		var lang string
		if msg.Lang != "" {
			lang = msg.Lang
		} else {
			lang = "en"
		}
		out, err := getWeather(msgContent, lang)
		if err != nil {
			replyBody = "Failed!"
		} else {
			replyBody = string(out)
		}
	}
	reply := xmpp.Chat{Remote: msg.Remote, Type: "chat", Text: replyBody}
	return reply, nil
}

func errorHandler(err error) {
	log.Println(err.Error())
}
